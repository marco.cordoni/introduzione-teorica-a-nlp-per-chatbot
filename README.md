# Introduzione teorica a NLP per ChatBot

Questo progetto contiene gli appunti in LaTex che ho appreso dal corso di Deep Learning [Deep Learning and NLP A-Z™: How to create a ChatBot](https://www.udemy.com/course/chatbot/).

### Prerequisiti

- Livello di matematica da scuola superiore
- Conoscenze basilari di programmazione
- Consigliato ma non fondamentale: aver già visto letto [Fondamenti di ML](https://gitlab.com/marco.cordoni/fondamenti-di-ml)

## Autore

  - **Marco Cordoni**
